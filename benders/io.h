/*
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <stdlib.h>

using namespace std;

//#define FORN(i, a, b) for(typeof(b) i = (a); i <= (b); i++)

class InputData
{
  private:
    void split(const string &s, char delim, vector<int> &elems);
    istream& safeGetline(istream& stream, string& t);

  public:
    InputData(string filePath);
    ~InputData();
    bool dataLoaded;
	void alfa(int task);
	int delta(int, int, int, int);

    int tasks;                    //number of tasks n
    int cranes;                   //number of cranes q 
    int bays;                     //number of bays b
    int time;                     //travel time t
    int margin;                   //safety margin s
    vector< pair<int,int> > Phi;  //set of precedence constrained task pairs
    vector< pair<int,int> > Psi;  //set of nonsimultaneous task pairs
    vector<int> P;                //processing time of each task
    vector<int> L;                //bay location of each task
    vector<int> R;                //ready time of each crane
    vector<int> L0;               //initial bay location of each crane
    vector<int> H;                //position in the vessel
    vector<int> O;                //operation to be done

    vector<int>* predec;             //predecessor list for each node
	vector<int> LB;
	vector<int> a_;

    friend ostream& operator<<(ostream& outstr, const InputData& id);
};

