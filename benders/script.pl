#!/usr/bin/perl

use strict;
use warnings;
use File::Find;
use Getopt::Long;

my @file;
my $result;
my $idx;
my @Files;
my $direc;
my $out;
my @instances;
my @instance;
my @execs;
my @exec;

$out = GetOptions ("d=s" => \$direc);
#print("$direc\n");
@instances = ("D");#("A","B","C","D");
@execs = ("benders.exe", "mono.exe");

foreach my $instance (@instances)
{
  opendir(Dir,"$direc/$instance");
  @Files = sort readdir(Dir);
  print("$instance\n");
  foreach my $file (@Files)
  {
    if(index($file,".qcs")>=0)
    {
      print("$file\n");
      foreach my $exec (@execs)
      {
        $result = `$exec $direc/$instance/$file`;
        $idx = index($result,"results:");
        print substr $result, $idx+8, 50;
        #print " && ";
        #print $result;
      }
      print "\n";
    }
  }
  closedir(Dir);
  sleep 1;
  #$result = `swapoff -a`;
  #$result = `swapon -a`;
}

