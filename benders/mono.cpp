#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <chrono>   //compatibility?

#include <gurobi_c++.h>

#include <stdio.h>
#include <string.h>

#include <algorithm>

#include "io.h"

#define MAXCRANES 6
#define MAXTASKS 30

using namespace std;

typedef struct _schedule
{
  GRBVar** z;  //task ordering variables
  GRBVar* D;   //completion time (task)
  GRBVar* C;   //completion time (crane)
  GRBVar W;    //makespan
} MDL_Schedule;

typedef struct _route
{
  GRBVar** x[MAXCRANES];  //arc variable
  vector<GRBVar**> xVars; //for callback
  GRBVar** y;             //task assignment
  GRBVar eta;
} MDL_Route;

int createModel(InputData&, GRBModel&, MDL_Route&, MDL_Schedule&, int);
double solveModel(InputData&, GRBModel&, MDL_Route&, MDL_Schedule&, vector< vector<int> >&, vector<int>&, const char*);

int MOD(int a) { return a>0 ? a : -a; }
int drawScheduleSVG(InputData& data, vector<int>& D, vector<int>& assigment, int makeSpan, const char*);

/*GLOBAL VARIABLES...*/
double** solutionT[MAXCRANES];
double** solutionA;
double** solutionZ;
double* solutionC;
double* solutionD;

vector<int> assigment(MAXTASKS);
vector<int> ord(MAXTASKS);
vector<int> finish;

int COUNT = 0;

class MyCallBack: public GRBCallback
{
  public:
    vector<GRBVar**> arcVars;
    GRBVar** xx;  //arc variable
    GRBVar** yy;
    InputData* data;
    MyCallBack(InputData* d, vector<GRBVar**>& arcsK, GRBVar** asgK)
    {
      arcVars = arcsK;
      yy = asgK;
      data = d;
    }
  protected:
    void callback()
    {
      try
      {
        if(where==GRB_CB_MIPSOL)
        {//try to find some subtour constraints for the x^k arc variables...
          int N = data->tasks;
          int K = data->cranes;
          int T = N+1;
          double** _x = new double*[N+1];
          double** _y = new double*[N];
          vector<int> S(N);
          vector<int> components(N+2);
          int nComps;
   
          bool secFound, precFound;
          vector<bool> visited(N);
          vector<int> path;

          for(int i=0; i<=N-1; i++)
            _y[i] = getSolution(yy[i], K);

          /*FORN(i,0,N-1)
            FORN(k,0,K-1)
              if(_y[i][k]>0.9)
              {
                cout<<"task "<<i+1<<" assigned to crane "<<k+1<<endl;
                k=K;
              }*/

                      
		  secFound = false;
          for(int q=0; q<=K-1; q++)
          {
            //cout<<"crane"<<q<<endl;
            xx = arcVars[q];
            for(int i=0; i<=N; i++)
              _x[i] = getSolution(xx[i], N+2);

            //memset(components, 0, sizeof(components));
			for(int i=0; i<N+2; i++)
			  components[i] = 0;

            int u = 0;
            bool flag = false;
            nComps = 0;
            components[0] = 1;
            path.clear();
            //cout<<"Path:"<<u<<" ";
            while(!flag)
            {
              for(int v=1; v<=T; v++)
                if(u!=v)
                  if(_x[u][v] > 0.9)
                  {
                    u = v; v = T+2;//TODO:check
                    if(u==T)
					  flag = true;
                    else
					  path.push_back(u);
                    components[u] = 1;
                    //cout<<u<<" ";
                  }
            }
            nComps++;
            //cout<<endl;
           
            for(int i=1; i<=N; i++)   //i in Tasks
            {
              if(components[i]==0 && _y[i-1][q]>0.9)
              {
                u = i;
                flag = false;
                nComps++;
                S.clear();
                while(!flag)
                {
                  for(int j=1; j<=T; j++)  //j in Tasks+T
                  {
                    if(u!=j)
                      if(_x[u][j]>0.9)
                      {
                        components[u] = nComps;
                        u = j; j = T+2;
                        S.push_back(u);
                        if(u==i)
                        {//found a subtour!
                          flag = true;
                          //cout<<"S:";
                          //FORN(ii,0,S.size())
                          //  cout<<S[ii]<<" ";
                          //cout<<endl;
                          addSEC(S, q);
						  secFound = true;
                          //cout<<"added for crane "<<q<<endl;
                        }
                      }
                  }//FORN_j
                }//while
              }//if
            }//FORN_i

            //===============Precedences===============
            //separate the first x_k(S)+y_ik<=|s|-1  0-->8------->4--->T
            S.clear();
            S.push_back(0);
            //memset(visited, false, sizeof(visited));
			for(int i=0; i<N; i++)
		      visited[i] = false;
            precFound = false;
            if(path.size()>=2)
            {
              //cout<<"Path size: "<<path.size()<<endl;
              for(int p=0; p<=path.size()-1; p++)
              {
                //cout<<path[p]<<" ";
                u = path[p];
                S.push_back(u);
                visited[u-1] = true;
                if(data->predec[u-1].size()>0)
                  for(int i=0; i<=data->predec[u-1].size()-1; i++)
                  {
                    if(_y[data->predec[u-1][i]-1][q]>0.9999 && !visited[data->predec[u-1][i]-1])
                    {
                      //cout<<u<<" before "<<data->predec[u-1][i]<<" on crane "<<q<<endl;//" "<<_y[data->predec[u-1][i]-1][q]<<endl;
                      addSECPrec(S,q,data->predec[u-1][i]-1);
                      precFound = true;
                    }
                  }
                if(precFound)
                  p = (int)path.size();
              }
              //cout<<endl;
            }
            //-----------------------------------------
          }//cranes loop
		  //cout<<endl;
        }
      }
      catch (GRBException e) {
        cout << "Error number: " << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;
      } catch (...) {
        cout << "Error during callback" << endl;
      }
    }
    
    void addSEC(vector<int>& subset, int q)
    {
      GRBLinExpr expr = 0;
      xx = arcVars[q];
      for(int i=0; i<=subset.size()-1; i++)
        for(int j=0; j<=subset.size()-1; j++)
          if(i!=j)
            expr += xx[subset[i]][subset[j]];
      addLazy(expr<=(int)subset.size()-1);
    }

    void addSECPrec(vector<int>& subset, int q, int u)
    {
      GRBLinExpr expr = 0;
      xx = arcVars[q];
      for(int i=0; i<=subset.size()-1; i++)
        for(int j=0; j<=subset.size()-1; j++)
          if(i!=j && j!=0)
            expr += xx[subset[i]][subset[j]];
      expr += yy[u][q];
      addLazy(expr<=(int)subset.size()-1);
      //cout<<expr<<endl;
    }
};

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    cout << "usage: \t" << argv[0] << " [data-file]" << endl;
    return 0;
  }

  try
  {
    InputData data(argv[1]);
	cout<<data;

    int N, K, t, d, T, B;

    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    vector< vector<int> > routes(K);
	//vector<int> assigment(N);

    for(int k=0; k<=K-1; k++)
      solutionT[k] = new double*[N+2];//TODO:N+1

	solutionA = new double*[N];
	for(int u=0; u<N; u++)
		solutionA[u] = new double[K];

	solutionZ = new double*[N];
	for(int u=0; u<N; u++)
		solutionZ[u] = new double[N];

	solutionC = new double[MAXCRANES];
	solutionD = new double[MAXTASKS];

	GRBEnv* env;
    env = new GRBEnv();
    //TODO: ckeck other paramters...
    env->set(GRB_DoubleParam_MIPGapAbs, 0.9999);
    env->set(GRB_IntParam_Seed, 13);
	env->set(GRB_IntParam_OutputFlag, 0);
	env->set(GRB_DoubleParam_TimeLimit, 7200);
	env->set(GRB_IntParam_DisplayInterval, 600);

	//cout<<"computing alfa...\n";
    for(int i=0; i<N; i++)
      data.alfa(i);

	int U = 0;
    for(int i=1; i<=N; i++)
      U += data.P[i-1];
    U += (B-1)*t;//(2*data.L0[0]-2)*t + (2*(B-data.L0[0]+1)-2)*t;

	cout<<"creating model...\n";
	GRBModel model = GRBModel(*env);
	
    MDL_Route routing;
	MDL_Schedule scheduling;
	createModel(data, model, routing, scheduling, U);
    for(int k=0; k<=K-1; k++)
		routing.xVars.push_back(routing.x[k]);

    /*routing.mdl->getEnv().set(GRB_IntParam_LazyConstraints, 1);
    MyCallBack callback = MyCallBack(&data,routing.xVars,routing.y);
    routing.mdl->setCallback(&callback);*/

	auto ti =chrono::high_resolution_clock::now();

	if(solveModel(data, model, routing, scheduling,routes, assigment, argv[1])<=0)
	{
		cout<<"Infeasible model...\n";
		return 1;
	}

    auto tf = chrono::high_resolution_clock::now();

	cout<<"Optimal soution found!\n";
	double otime = (double)(chrono::duration_cast<chrono::milliseconds>(tf-ti).count())/1000;
	cout<<"Total time: "<<otime<<"s"<<endl;
	cout<<"results: "<<otime<<" & "<<model.get(GRB_DoubleAttr_MIPGap)<<endl;
  }
  catch(const invalid_argument& e)
  { cout<<e.what(); }
  catch(GRBException ex)
  { cout<<"Gurobi "<<ex.getErrorCode()<<"exception caught! "<<ex.getMessage()<<endl;}
  return 0;
}

int createModel(InputData& data, GRBModel& mdl, MDL_Route& mr, MDL_Schedule& ms, int M)
{
    int N, K, t, d, T, B, aux;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

	ostringstream str;

    /**Variables declarations*/
    for(int k=0; k<=K-1; k++)     //k in Cranes
    {
      mr.x[k] = new GRBVar*[N+1];    //x^k_i i in Tasks+0
      for(int i=0; i<=N; i++)     //i in Tasks+0
      {
        mr.x[k][i] = mdl.addVars(N+2, GRB_BINARY);
        mdl.update();
        for(int j=1; j<=T; j++)   //j in Tasks+T
        {
          str<<"x_("<<i<<","<<j<<","<<k+1<<")";
          mr.x[k][i][j].set(GRB_StringAttr_VarName, str.str());
          //cout<<str.str()<<endl;
          str.str("");
          str.clear();
          if(i==j)
            mr.x[k][i][j].set(GRB_DoubleAttr_UB, 0.0);
        }
      }
      for(int i=0; i<=N; i++)     //i in Tasks+0
        mr.x[k][i][0].set(GRB_DoubleAttr_UB, 0.0);
    }

    mr.y = new GRBVar*[N];
    for(int i=0; i<=N-1; i++)
    {
      mr.y[i] = mdl.addVars(K, GRB_BINARY);
      mdl.update();
      for(int k=0; k<=K-1; k++)
      {
        str<<"y_("<<i+1<<","<<k+1<<")";
        mr.y[i][k].set(GRB_StringAttr_VarName, str.str());
        str.str("");
        str.clear();
      }
    }

    mdl.update();
    
    /**Constraints declarations*/
    GRBLinExpr expr = 0;

    //out degree 0
    for(int k=0; k<=K-1; k++)  //one ctr. for each crane
    {
      expr = 0;
      for(int j=1; j<=T; j++)  //j in Tasks+T
        expr += mr.x[k][0][j];
      mdl.addConstr(expr==1);
    }

    //in degree T
    for(int k=0; k<=K-1; k++)  //one ctr. for each crane
    {
      expr = 0;
      for(int j=0; j<=N; j++)  //j in Tasks+0
        expr += mr.x[k][j][T];
      mdl.addConstr(expr==1);
    }

    //assignment constraints
    for(int i=1; i<=N; i++)       //i in Tasks
      for(int k=0; k<=K-1; k++)
      {
        expr = 0;
        for(int j=1; j<=T; j++)   //j in Tasks+T
          if(i!=j)
            expr += mr.x[k][i][j];
        mdl.addConstr(expr-mr.y[i-1][k]==0);

        expr = 0;
        for(int j=0; j<=N; j++)   //j in Tasks+0
          if(i!=j)
            expr += mr.x[k][j][i];
        mdl.addConstr(expr-mr.y[i-1][k]==0);
      }

    //flow conservation
    for(int i=0; i<=N-1; i++) //i in Tasks
    {
      expr = 0;
      for(int k=0; k<=K-1; k++)
        expr += mr.y[i][k];
      mdl.addConstr(expr==1);
    }

    //SECS for S: |S|=2
    for(int i=1; i<=N; i++)  //i,j in Tasks
      for(int j=i+1; j<=N; j++)
        if(i!=j)
        {
          for(int k=0; k<=K-1; k++)
          {
            expr = mr.x[k][i][j] + mr.x[k][j][i];
            mdl.addConstr(expr<=1);
          }
        }

	//precedence cycle?
    for(int i=0; i<N; i++)
	{
       for(int j=i+1; j<N; j++)
	   {
		   if(data.predec[i].size()>0 && data.predec[j].size()>0 && data.L[i]!=data.L[j])
		   {
			   for(vector<int>::iterator ii=data.predec[i].begin(); ii!=data.predec[i].end(); ++ii)
				   for(vector<int>::iterator jj=data.predec[j].begin(); jj!=data.predec[j].end(); ++jj)
					   for(int k=0; k<K; k++)
					   {
					     expr = mr.x[k][*ii][j+1]+mr.x[k][*jj][i+1];
						 mdl.addConstr(expr<=1);
						 //cout<<expr<<endl;
					   }
		   }
	   }
	}

	/*impose lower and upper bay limits for a crane*/
	int min, max;
	for(int k=1; k<=K; k++)
	{
	  min = (k-1)*(d+1)+1;
	  max = B - (K-k)*(1+d);
	  expr = 0;
	  for(int i=0; i<N; i++)
		  if(data.L[i]<min || data.L[i]>max)
		    expr += mr.y[i][k-1];
	  mdl.addConstr(expr==0);
	}
	
    mdl.update();

	//scheduling variables
    ms.W = mdl.addVar(0, M, 1, GRB_CONTINUOUS, "W");  //makespan
    //D = mdl.addVars(N, GRB_INTEGER);//TODO:INTEGER?
    ms.D = mdl.addVars(NULL, NULL, NULL, NULL, NULL, N);
	ms.C = mdl.addVars(K, GRB_CONTINUOUS);//TODO:INTEGER?

    ms.z = new GRBVar*[N];
    for(int i=1; i<=N; i++)
    {
      ms.z[i-1] = mdl.addVars(N, GRB_BINARY);
      mdl.update();
      for(int j=1; j<=N; j++)
      {
        str<<"z_("<<i<<","<<j<<")";
        ms.z[i-1][j-1].set(GRB_StringAttr_VarName, str.str());
        str.str("");
        str.clear();
        if(i==j)
          ms.z[i-1][j-1].set(GRB_DoubleAttr_UB,0.0);
      }
    }

    mdl.update();
    for(int i=0; i<N; i++)
	{
	  ms.D[i].set(GRB_DoubleAttr_Obj, 0);
	  ms.D[i].set(GRB_CharAttr_VType, GRB_CONTINUOUS);
	  //C[k].set(GRB_DoubleAttr_LB, 0.0);
	  //C[k].set(GRB_DoubleAttr_UB, M);
      str<<"D_("<<i+1<<")";
      ms.D[i].set(GRB_StringAttr_VarName, str.str());
      str.str("");
      str.clear();
	}

	for(int k=0; k<K; k++)
	{
      str<<"C_("<<k+1<<")";
      ms.C[k].set(GRB_StringAttr_VarName, str.str());
      str.str("");
      str.clear();
	}


    //s.t. nonSim{(i,j) in Psi}: z[i,j] + z[j,i] = 1;
    for(int i=1; i<=N; i++)
      for(int j=i+1; j<=N; j++)
        if(MOD(data.L[i-1]-data.L[j-1])<=data.margin && MOD(data.L[i-1]-data.L[j-1])>0)
        {
          expr = ms.z[i-1][j-1]+ms.z[j-1][i-1];
          mdl.addConstr(expr==1);
        }

    //s.t. precedence relations (i,j) => z_ij=1 z_ji=0
    for(vector< pair<int,int> >::iterator ele = data.Phi.begin(); ele!=data.Phi.end(); ++ele)
    {
      //cout<<ele->first<<"<"<<ele->second<<endl;
      ms.z[ele->first-1][ele->second-1].set(GRB_DoubleAttr_LB, 1.0);
      ms.z[ele->first-1][ele->second-1].set(GRB_DoubleAttr_UB, 1.0);
      ms.z[ele->second-1][ele->first-1].set(GRB_DoubleAttr_LB, 0.0);
      ms.z[ele->second-1][ele->first-1].set(GRB_DoubleAttr_UB, 0.0);
    }

//task schedulling...
    //s.t. _cc1{i in Tasks, j in Tasks, k in Cranes: i!=j}: D[i] + t*abs(l[i]-l[j]) + p[j] - D[j] <= M*(1-_x[i,j,k]);
    for(int i=1; i<=N; i++)
      for(int j=1; j<=N; j++)
        if(i!=j)
          for(int k=0; k<=K-1; k++)
          {
			//if(mr.x[k][i][j]>0.1)
			{
		      str<<"R1_"<<i<<","<<j;
			  aux = t*MOD(data.L[i-1]-data.L[j-1]) - data.a_[j-1];
              expr = ms.D[i-1]+t*MOD(data.L[i-1]-data.L[j-1])+data.P[j-1]-ms.D[j-1];
			  mdl.addConstr(expr<=M*(1-mr.x[k][i][j]), str.str());
			  str.str("");
              str.clear();
			}
          }

    //s.t. _cc2{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[i] + p[j] - D[j] <= M*(1-z[i,j]);
    //s.t. _cc3{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[j] - p[j] - D[i] <= M*z[i,j];
    for(int i=1; i<=N; i++)
      for(int j=1; j<=N; j++)
      {
        if(i!=j)
        {
          str<<"R2_"<<i<<","<<j;
          expr = ms.D[i-1] + data.P[j-1] - ms.D[j-1] + M*ms.z[i-1][j-1];
          mdl.addConstr(expr<=M, str.str());
		  str.str("");
          str.clear();

		  str<<"R3_"<<i<<","<<j;
          expr = ms.D[j-1] - data.P[j-1] - ms.D[i-1] - (M-data.P[j-1]-data.P[i-1])*ms.z[i-1][j-1];//(M-data.P[j-1]-data.a_[i-1]-data.P[i-1])
          mdl.addConstr(expr<=0, str.str());
		  str.str("");
          str.clear();
        }
      }

    //s.t. _cc6{j in Tasks, k in Cranes:lT[k]!=0}: D[j] + t*abs(l[j]-lT[k]) - C[k] <= M*(1-_x[j,T,k]);
    for(int j=1; j<=N; j++)  //(l_j-lT_k)==0
      for(int k=0; k<=K-1; k++)
      {
        expr = ms.D[j-1] - ms.C[k];
		str<<"R6_"<<j<<","<<k;
        mdl.addConstr(expr<=M*(1-mr.x[k][j][T]), str.str());
		str.str("");
        str.clear();
      }

	//cout<<"lower bound on starting time\n";
	/*for(int i=0; i<N; i++)
	  cout<<data.a_[i]<<" ";
	cout<<endl;*/
    //s.t. _cc8{j in Tasks, k in Cranes}: r[k] - D[j] + t*abs(l0[k]-l[j]) + p[j] <= M*(1-_x[O,j,k]);
    for(int j=1; j<=N; j++)
      for(int k=0; k<=K-1; k++)
      {
		str<<"R8";
        expr = data.R[k] - ms.D[j-1] + t*MOD(data.L0[k]-data.L[j-1]) + data.P[j-1];
		mdl.addConstr(expr<=(data.R[k]+t*MOD(data.L0[k]-data.L[j-1])-data.a_[j-1])*(1-mr.x[k][0][j]), str.str());
		str.str("");
        str.clear();
      }

    //s.t. cc9{k in Cranes}: C[k]<=W;
    for(int k=0; k<=K-1; k++)
	{
	  str<<"R9_"<<k;
      mdl.addConstr(ms.C[k]<=ms.W, str.str());
      str.str("");
      str.clear();
	}

    //s.t. cc10{i in Tasks}: D[i]<=U;
    for(int i=1; i<=N; i++)
	{
	  str<<"R10a_"<<i;
      mdl.addConstr(ms.D[i-1]<=M, str.str());
	  str.str("");
      str.clear();
	  str<<"R10b_"<<i;
	  mdl.addConstr(ms.D[i-1]>=data.LB[i-1]+data.P[i-1], str.str());
	  str.str("");
      str.clear();
	}

	//minimum temporal distance (Bierwirth, Meisel)
	for(int i=1; i<=N; i++)
	{
	  for(int j=i+1; j<=N; j++)
	  {
		for(int v=1; v<=K; v++)
		  for(int w=1; w<=K; w++)
		  {
			aux = data.delta(i,j,v,w);
		    if(v!=w && aux>0)
			{
			  //cout<<i<<" "<<j<<" "<<v<<" "<<w<<"="<<aux<<endl;
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += mr.y[i-1][v-1] + mr.y[j-1][w-1];
			  expr += ms.z[i-1][j-1];
			  expr = (M+aux)*expr;
			  expr += ms.D[i-1] + aux + data.P[j-1] - ms.D[j-1];
			  str<<"RT_1_"<<i<<","<<j<<","<<v<<","<<w;
			  mdl.addConstr(expr<=3*(M+aux), str.str());
         	  str.str("");
              str.clear();
			  //
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += mr.y[i-1][v-1] + mr.y[j-1][w-1];
			  expr += ms.z[j-1][i-1];
			  expr = (M+aux)*expr;
			  expr += ms.D[j-1] + aux + data.P[i-1] - ms.D[i-1];
			  str<<"RT_2_"<<i<<","<<j<<","<<v<<","<<w;
			  mdl.addConstr(expr<=3*(M+aux), str.str());
         	  str.str("");
              str.clear();
			  //--------------------------
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += mr.y[i-1][v-1] + mr.y[j-1][w-1];
			  expr += -ms.z[i-1][j-1]-ms.z[j-1][i-1];
			  //str<<"RT_3_"<<i<<","<<j<<","<<v<<","<<w;
			  mdl.addConstr(expr<=1, str.str());
         	  str.str("");
              str.clear();
			}
		  }
	  }
	}

	//valid inequality (51)
	for(int k=0; k<K; k++)
	{
	  expr = data.R[k];
	  for(int i=1; i<=N; i++)
	  {
		{
		  expr += (data.LB[i-1])*mr.x[k][0][i];//+ data.LB[i-1]-data.P[i-1]
		  //cout<<"x("<<k<<","<<0<<","<<i<<") "<<t*(MOD(data.L0[k]-data.L[i-1]))<<" "<<data.LB[i-1]<<endl;
		}

		for(int j=1; j<=N; j++)
		  if(i!=j)
			{
		      expr += (data.P[i-1] + t*MOD(data.L[i-1]-data.L[j-1]))*mr.x[k][i][j];
			  //cout<<"x("<<k<<","<<i<<","<<j<<") "<<data.P[i-1]<<" "<<t*MOD(data.L[i-1]-data.L[j-1])<<endl;
			}

		{
		  expr += data.P[i-1]*mr.x[k][i][T];
		  //cout<<"x("<<k<<","<<i<<","<<T<<") "<<data.P[i-1]<<endl;
		}
	  }
	  mdl.addConstr(ms.C[k]>=expr);
	}

    mdl.update();
    mdl.write("formulation.lp");
	return 1;
}

double solveModel(InputData& data, GRBModel& mdl, MDL_Route& mr, MDL_Schedule& ms, vector< vector<int> >& routes, vector<int>& assigment, const char* name)
{
    int N, K, t, d, T, B;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    mdl.optimize();
    double ret = 0;
    if(mdl.get(GRB_IntAttr_SolCount)>0)
    {
      for(int k=0; k<=K-1; k++)
        for(int u=0; u<=N; u++)
          solutionT[k][u] = mdl.get(GRB_DoubleAttr_X, mr.x[k][u], N+2);

	  for(int u=0; u<N; u++)
	   solutionA[u] = mdl.get(GRB_DoubleAttr_X, mr.y[u], K);

      int u;
      bool flag;
      for(int k=0; k<=K-1; k++)
      {
        cout<<"Task sequence for crane "<<k<<endl;
        u = 0; flag = false;
        while(!flag)
        {
          for(int j=1; j<=T; j++)
          {
            if(solutionT[k][u][j]>0.9)
            {
              cout<<"x_("<<u<<","<<j<<","<<k+1<<")="<<solutionT[k][u][j]<<endl;
              u = j; j = T+1;
              if(u==T)
                flag=true;
              else
			  {
                assigment[u-1] = k;
				routes[k].push_back(u);
			  }
            }
          }
        }
      }
      double makespan;

	  finish.clear();

	  //scheduling solution
      solutionC[0] = ms.W.get(GRB_DoubleAttr_X);
      cout<<"Makespan="<<solutionC[0]<<endl;
      makespan = solutionC[0];

      solutionC = mdl.get(GRB_DoubleAttr_X, ms.C, K);
      for(int k=1; k<=K; k++)
        cout<<"C_"<<k<<"="<<solutionC[k-1]<<endl;

      solutionD = mdl.get(GRB_DoubleAttr_X, ms.D, N);
      for(int i=1; i<=N; i++)
      {
        cout<<"D_"<<i<<"="<<solutionD[i-1]<<endl;
        finish.push_back((int)(solutionD[i-1]+0.5));
      }

      for(int i=0; i<=N-1; i++)
      {
        solutionZ[i] = mdl.get(GRB_DoubleAttr_X, ms.z[i], N);
        //for(int j=0; j<=N-1; j++)
        //  if(solutionZ[i][j]>0)
        //    cout<<"z_("<<i+1<<","<<j+1<<")="<<solutionZ[i][j]<<endl;
      }

      drawScheduleSVG(data, finish, assigment, (int)(makespan), name);

	  return makespan;
    }//if soluton found....
	return -100.0;
}

int drawScheduleSVG(InputData& data, vector<int>& D, vector<int>& assigment, int makeSpan, const char* inst)
{
  int yMax;
  string colors[10]={"#dcdcdc", "#a52a2a", "#006400", "#ffa500", "#ff0000",
                     "#ffff00", "#ff4500", "#008000", "#ffebeb", "#808080" };

  string name(inst);
  name = name.substr(0, name.rfind("."));
  ostringstream str;
  str<<name<<"_"<<++COUNT<<".svg";
  //cout<<inst<<endl;
  //cout<<str.str()<<endl;
  ofstream svgFile(str.str());

  yMax = data.bays*55;
  svgFile<<"<?xml version=\"1.0\" standalone=\"yes\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">\n";
  svgFile<<"<svg xmlns=\"http://www.w3.org/2000/svg\">\n";
  //draws the time axis
  svgFile<<"<line x1=\"0\"  y1=\""<<yMax<<"\" x2=\""<<10*makeSpan+55<<"\" y2=\""<<yMax<<"\" style=\"stroke:#000000; stroke-width: 6px;\"/>\n";

  //draws a vertical line for each task completion time
  for(int i=0; i<=data.tasks-1; i++)
  {
   svgFile<<"<line x1=\""<<10*D[i]<<"\" y1=\""<<yMax<<"\" x2=\""<<10*D[i]<<"\"   y2=\"0\" style=\"stroke:#000000; stroke-width: 2px; stroke-dasharray: 10 5;\"/>\n";
   svgFile<<"<text fill=\"BLACK\" font-size=\"30\" x=\""<<10*D[i]<<"\" y=\""<<yMax+20<<"\">"<<D[i]<<"</text>\n";
  }

  //draws a rectangle for each task and the task number inside it
  int start;
  for(int i=0; i<=data.tasks-1; i++)
  {
    start = D[i]-data.P[i];    
    svgFile<<"<rect rx=\"13\" x=\""<<10*start<<"\" y=\""<<(data.L[i]-1)*55<<"\" height=\"50\" width=\""<<10*data.P[i]<<"\" style=\"fill:"<<colors[assigment[i]]<<"\"/>\n";
    svgFile<<"<text fill=\"black\" font-size=\"30\" x=\""<<10*data.P[i]/2 + 10*start<<"\" y=\""<<(data.L[i]-1)*55+35<<"\">"<<i+1<<"</text>\n";
  }
  svgFile<<"</svg>\n";
  return 0;
}
