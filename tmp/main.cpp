#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <chrono>   //compatibility?

#include <gurobi_c++.h>

#include <stdio.h>
#include <string.h>

#include <algorithm>

#include "io.h"

#define MAXCRANES 6
#define MAXTASKS 30
#define TIMELIMIT 7200

using namespace std;

typedef struct _schedule
{
  GRBModel* mdl;
  GRBVar** z;  //task ordering variables
  GRBVar* D;   //completion time (task)
  GRBVar* C;   //completion time (crane)
  GRBVar W;    //makespan

  _schedule(GRBEnv* env) { mdl = new GRBModel(*env); };
} MDL_Schedule;


typedef struct _route
{
  GRBModel* mdl;
  GRBVar** x[MAXCRANES];  //arc variable
  vector<GRBVar**> xVars; //for callback
  GRBVar** y;             //task assignment
  GRBVar eta;

  _route(GRBEnv* env) { mdl = new GRBModel(*env); };
} MDL_Route;

int createRoutingModel(InputData&, MDL_Route&);
double solveRoutingModel(InputData&, MDL_Route&, vector< vector<int> >&, vector<int>&);
int addNoGood(InputData&, MDL_Route&, vector< vector<int> >&);
int addNoGoodCrane(InputData&, MDL_Route&, vector<int>&, int);
int addPrecCut(InputData&, MDL_Route&, vector<int>&, vector<int>&, int, int, int, int);
int addFeasCut(InputData&, MDL_Route&, vector<int>&, vector<int>&, int, int, int, int, int, int);

int createSchedulligModel(InputData&, MDL_Schedule&, vector< vector<int> >&, int);
bool solveSchedullingModel(InputData&, MDL_Schedule&, int&);

bool findCuts(InputData&, MDL_Route&, vector< vector<int> >&, vector<int>&, int);
bool findCutsMaster(InputData&, MDL_Route&, vector< vector<int> >&, vector<int>&);

int MOD(int a) { return a>0 ? a : -a; }
int drawScheduleSVG(InputData& data, vector<int>& D, vector<int>& assigment, int makeSpan, const char*);

/*GLOBAL VARIABLES...*/
double** solutionT[MAXCRANES];
double** solutionA;
double** solutionZ;
double* solutionC;
double* solutionD;

vector<int> assigment(MAXTASKS);
vector<int> ord(MAXTASKS);
vector<int> finish;

int COUNT = 0;
int noGood = 0;
int fsCuts = 0;
int pcCuts = 0;

struct _compD
{
	bool operator() (int i, int j) {return solutionD[i]<solutionD[j];}
} compD;

class MyCallBack: public GRBCallback
{
  public:
    vector<GRBVar**> arcVars;
    GRBVar** xx;  //arc variable
    GRBVar** yy;
    InputData* data;
    MyCallBack(InputData* d, vector<GRBVar**>& arcsK, GRBVar** asgK)
    {
      arcVars = arcsK;
      yy = asgK;
      data = d;
    }
  protected:
    void callback()
    {
      try
      {
        if(where==GRB_CB_MIPSOL)
        {//try to find some subtour constraints for the x^k arc variables...
          int N = data->tasks;
          int K = data->cranes;
          int T = N+1;
          double** _x = new double*[N+1];
          double** _y = new double*[N];
          vector<int> S(N);
          vector<int> components(N+2);
          int nComps;
   
          bool secFound, precFound;
          vector<bool> visited(N);
          vector<int> path;

          for(int i=0; i<=N-1; i++)
            _y[i] = getSolution(yy[i], K);

          /*FORN(i,0,N-1)
            FORN(k,0,K-1)
              if(_y[i][k]>0.9)
              {
                cout<<"task "<<i+1<<" assigned to crane "<<k+1<<endl;
                k=K;
              }*/

                      
		  secFound = false;
          for(int q=0; q<=K-1; q++)
          {
            //cout<<"crane"<<q<<endl;
            xx = arcVars[q];
            for(int i=0; i<=N; i++)
              _x[i] = getSolution(xx[i], N+2);

            //memset(components, 0, sizeof(components));
			for(int i=0; i<N+2; i++)
			  components[i] = 0;

            int u = 0;
            bool flag = false;
            nComps = 0;
            components[0] = 1;
            path.clear();
            //cout<<"Path:"<<u<<" ";
            while(!flag)
            {
              for(int v=1; v<=T; v++)
                if(u!=v)
                  if(_x[u][v] > 0.9)
                  {
                    u = v; v = T+2;//TODO:check
                    if(u==T)
					  flag = true;
                    else
					  path.push_back(u);
                    components[u] = 1;
                    //cout<<u<<" ";
                  }
            }
            nComps++;
            //cout<<endl;
           
            for(int i=1; i<=N; i++)   //i in Tasks
            {
              if(components[i]==0 && _y[i-1][q]>0.9)
              {
                u = i;
                flag = false;
                nComps++;
                S.clear();
                while(!flag)
                {
                  for(int j=1; j<=T; j++)  //j in Tasks+T
                  {
                    if(u!=j)
                      if(_x[u][j]>0.9)
                      {
                        components[u] = nComps;
                        u = j; j = T+2;
                        S.push_back(u);
                        if(u==i)
                        {//found a subtour!
                          flag = true;
                          //cout<<"S:";
                          //FORN(ii,0,S.size())
                          //  cout<<S[ii]<<" ";
                          //cout<<endl;
                          addSEC(S, q);
						  secFound = true;
                          //cout<<"added for crane "<<q<<endl;
                        }
                      }
                  }//FORN_j
                }//while
              }//if
            }//FORN_i

            //===============Precedences===============
            //separate the first x_k(S)+y_ik<=|s|-1  0-->8------->4--->T
            S.clear();
            S.push_back(0);
            //memset(visited, false, sizeof(visited));
			for(int i=0; i<N; i++)
		      visited[i] = false;
            precFound = false;
            if(path.size()>=2)
            {
              //cout<<"Path size: "<<path.size()<<endl;
              for(int p=0; p<=path.size()-1; p++)
              {
                //cout<<path[p]<<" ";
                u = path[p];
                S.push_back(u);
                visited[u-1] = true;
                if(data->predec[u-1].size()>0)
                  for(int i=0; i<=data->predec[u-1].size()-1; i++)
                  {
                    if(_y[data->predec[u-1][i]-1][q]>0.9999 && !visited[data->predec[u-1][i]-1])
                    {
                      //cout<<u<<" before "<<data->predec[u-1][i]<<" on crane "<<q<<endl;//" "<<_y[data->predec[u-1][i]-1][q]<<endl;
                      addSECPrec(S,q,data->predec[u-1][i]-1);
                      precFound = true;
                    }
                  }
                if(precFound)
                  p = (int)path.size();
              }
              //cout<<endl;
            }
            //-----------------------------------------
          }//cranes loop
		  //cout<<endl;
        }
      }
      catch (GRBException e) {
        cout << "Error number: " << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;
      } catch (...) {
        cout << "Error during callback" << endl;
      }
    }
    
    void addSEC(vector<int>& subset, int q)
    {
      GRBLinExpr expr = 0;
      xx = arcVars[q];
      for(int i=0; i<=subset.size()-1; i++)
        for(int j=0; j<=subset.size()-1; j++)
          if(i!=j)
            expr += xx[subset[i]][subset[j]];
      addLazy(expr<=(int)subset.size()-1);
    }

    void addSECPrec(vector<int>& subset, int q, int u)
    {
      GRBLinExpr expr = 0;
      xx = arcVars[q];
      for(int i=0; i<=subset.size()-1; i++)
        for(int j=0; j<=subset.size()-1; j++)
          if(i!=j && j!=0)
            expr += xx[subset[i]][subset[j]];
      expr += yy[u][q];
      addLazy(expr<=(int)subset.size()-1);
      //cout<<expr<<endl;
    }
};

int main(int argc, char **argv)
{
  if(argc != 2)
  {
    cout << "usage: \t" << argv[0] << " [data-file]" << endl;
    return 0;
  }

  try
  {
    InputData data(argv[1]);
	cout<<data;

    int N, K, t, d, T, B, iter, makespan, lb;
	bool flag;
	double ttime;

    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    vector< vector<int> > routes(K);
	//vector<int> assigment(N);

    for(int k=0; k<=K-1; k++)
      solutionT[k] = new double*[N+2];//TODO:N+1

	solutionA = new double*[N];
	for(int u=0; u<N; u++)
		solutionA[u] = new double[K];

	solutionZ = new double*[N];
	for(int u=0; u<N; u++)
		solutionZ[u] = new double[N];

	solutionC = new double[MAXCRANES];
	solutionD = new double[MAXTASKS];

	GRBEnv* env_route;
    env_route = new GRBEnv();
    //TODO: ckeck other paramters...
    env_route->set(GRB_DoubleParam_MIPGapAbs, 0.9999);
    env_route->set(GRB_IntParam_Seed, 13);
	env_route->set(GRB_IntParam_OutputFlag, 0);
	env_route->set(GRB_DoubleParam_TimeLimit, TIMELIMIT);
	env_route->set(GRB_IntParam_DisplayInterval, 600);

	//cout<<"computing alfa...\n";
    for(int i=0; i<N; i++)
      data.alfa(i);

	cout<<"creating routing model...\n";
    MDL_Route routing(env_route);
	createRoutingModel(data, routing);
    for(int k=0; k<=K-1; k++)
		routing.xVars.push_back(routing.x[k]);
    routing.mdl->getEnv().set(GRB_IntParam_LazyConstraints, 1);
    MyCallBack callback = MyCallBack(&data,routing.xVars,routing.y);
    routing.mdl->setCallback(&callback);


	GRBEnv* env_schedule;
    env_schedule = new GRBEnv();
    //TODO: ckeck other paramters...
    env_schedule->set(GRB_DoubleParam_MIPGapAbs, 0.9999);
    env_schedule->set(GRB_IntParam_Seed, 13);
	env_schedule->set(GRB_IntParam_OutputFlag, 0);
	env_route->set(GRB_DoubleParam_TimeLimit, TIMELIMIT);
	env_schedule->set(GRB_IntParam_DisplayInterval, 100);

	/*vector<int> tasksID;
	for(int i=0; i<N; i++)
		tasksID.push_back(i);
	bool cranesT[MAXCRANES];*/

	auto ti =chrono::high_resolution_clock::now();

	int U = 0;
    for(int i=1; i<=N; i++)
      U += data.P[i-1];
    U += (B-1)*t;//(2*data.L0[0]-2)*t + (2*(B-data.L0[0]+1)-2)*t;
	U = 99999;

	int UB = 99999;//U;
        cout<<"solving routing model...\n";
	int LB = (int)solveRoutingModel(data, routing, routes, assigment);
	cout<<"Initial LB:"<<LB<<endl;

    if(routing.mdl->get(GRB_IntAttr_SolCount)<=0)
	{ cout<<"Routing is infeasible...\n"; exit(1); }

	ttime = routing.mdl->get(GRB_DoubleAttr_Runtime);
    if(ttime>=TIMELIMIT)
    { lb=LB; LB=UB; }
	cout<<"----------------------------------\n";

	iter = 0;
	while(LB<UB)
	{
	  cout<<"creating schedulling model..."<<UB<<" "<<U<<endl;
      MDL_Schedule schedulling(env_schedule);
	  createSchedulligModel(data, schedulling, routes, U);//TODO:UB-1
      cout<<"solving schedulling model...";
	  flag = solveSchedullingModel(data, schedulling, makespan);
	  cout<<makespan<<endl;
	  //drawScheduleSVG(data, finish, assigment, (int)(makespan));//TEST

	  if(flag)
	  {
	    if(makespan < UB)
		{
		  cout<<"UB updated!\n";
		  UB = makespan;
		  drawScheduleSVG(data, finish, assigment, (int)(makespan), argv[1]);
		  /***********************************************************/
		}
	  }
	  else
	  {
	    cout<<"infeasible schedule...\n";
		schedulling.mdl->write("infeas.lp");
    	for(int i=0; i<=N; i++)
	     for(int j=1; j<=T; j++)
	       for(int k=0; k<K; k++)
		     if(i!=j)
			   if(solutionT[k][i][j]>0.1)
			     cout<<"x["<<k<<"]["<<i<<"]["<<j<<"]="<<solutionT[k][i][j]<<endl;
		cout<<endl;
    	for(int i=0; i<N; i++)
	     for(int j=0; j<N; j++)
		   if(solutionZ[i][j]>0.1)
			  cout<<"z["<<i+1<<"]["<<j+1<<"]="<<solutionZ[i][j]<<endl;

        schedulling.mdl->computeIIS();
        GRBConstr *constr = schedulling.mdl->getConstrs();
        int numCtrs = schedulling.mdl->get(GRB_IntAttr_NumConstrs);
        for(int i=0; i<=numCtrs-1; i++)
          if(constr[i].get(GRB_IntAttr_IISConstr))
           cout<<constr[i].get(GRB_StringAttr_ConstrName)<<endl;
        schedulling.mdl->write("iis.ilp");

		cout<<"results: "<<LB<<" INF"<<endl;
		exit(1);
	  }

	  cout<<iter<<" | "<< LB << " | " << UB << " | "<<100*(double)(UB-LB)/UB<<"% "<<endl;

	  //look for cuts...
	  if(!findCuts(data, routing, routes, assigment, UB))
	  {
		  /*sort(tasksID.begin(), tasksID.end(), compD);
		  for(int i=0;i<tasksID.size();i++)
			  cout<<tasksID[i]+1<<" ";
		  cout<<endl;
		  for(int i=0;i<N;i++)
			  cout<<solutionD[i]<<" ";
		  cout<<endl;

		  for(int i=0; i<K; i++)
			  cranesT[i] = false;
		  for(int i=0;i<tasksID.size();i++)
		  {
			  if(solutionD[i]>UB && !cranesT[assigment[tasksID[i]]])
			  {
				  cranesT[assigment[tasksID[i]]] = true;
				  cout<<i+1<<" ";
			  }
		  }
		  cout<<endl;*/
		    
	      addNoGood(data, routing, routes);
	  }

      //TODO:destruir o modelo Schedule
	  cout<<"----------------------------------\n";

	  if(LB<UB)
	  {
	    cout<<"solving routing model...";
		bool infeas = true;
		while(infeas)
		{
  	      for(int k=0; k<K; k++)
	        routes[k].clear();
          env_route->set(GRB_DoubleParam_TimeLimit, TIMELIMIT-ttime);
	      LB = (int)(solveRoutingModel(data, routing, routes, assigment)+0.5);
		  ttime += routing.mdl->get(GRB_DoubleAttr_Runtime);
		  cout<<LB<<endl;
	      ++iter;
		  infeas = findCutsMaster(data,routing,routes,assigment);
		}
	  }

	  lb = LB;
	  if(ttime>7200)  //timelimit on overall optimization
	    LB = UB;
	}
    auto tf = chrono::high_resolution_clock::now();
	double otime = (double)(chrono::duration_cast<chrono::milliseconds>(tf-ti).count())/1000;
	cout<<"Total time: "<<otime<<"s"<<endl;

	cout<<"finishing...\n";
	cout<<"last routing: "<<lb<<endl;
	cout<<"Makespan: "<<UB<<endl;
	cout<<"results: "<<lb<<" & "<<UB<<" & "<<otime<<" & "<<iter<<" & "<<noGood<<" & "<<fsCuts<<" & "<<pcCuts<<endl;
  }
  catch(const invalid_argument& e)
  { cout<<e.what(); }
  catch(GRBException ex)
  { cout<<"Gurobi "<<ex.getErrorCode()<<"exception caught! "<<ex.getMessage()<<endl;}
  return 0;
}


bool findCutsMaster(InputData& data, MDL_Route& routingMDL, vector< vector<int> >& routes, vector<int>& assigment)
{
	int i,j,l,m;
	int k1, k2;
	bool flag1, flag2, flag3;
	int seq;
	vector<int> S1;
	vector<int> S2;

	for(int k=0;k<data.cranes;k++)
	{
	  seq = 0;
      for (vector<int>::iterator it=routes[k].begin(); it!=routes[k].end(); ++it)
	    ord[*it-1] = seq++;
	}

	for(vector<pair<int, int> >::iterator ii=data.Phi.begin(); ii!=data.Phi.end(); ++ii)
	{
		for(vector<pair<int, int> >::iterator jj=data.Phi.begin(); jj!=data.Phi.end(); ++jj)
		{
			i=ii->first; j=ii->second; l=jj->first; m=jj->second;
			flag1 = i!=l && i!=m && j!=l && j!=m;
			flag2 = assigment[i-1]==assigment[m-1] && assigment[j-1]==assigment[l-1] && assigment[i-1]!=assigment[j-1] && assigment[l-1]!=assigment[m-1];
			flag3 = ord[j-1]<ord[l-1] && ord[m-1]<ord[i-1];
			if(flag1 && flag2 && flag3)
			{
			  /*cout<<"("<<i<<","<<ord[i-1]<<","<<assigment[i-1]<<") ";
			  cout<<"("<<j<<","<<ord[j-1]<<","<<assigment[j-1]<<") ";
			  cout<<"("<<l<<","<<ord[l-1]<<","<<assigment[l-1]<<") ";
			  cout<<"("<<m<<","<<ord[m-1]<<","<<assigment[m-1]<<") ";*/

			  k1 = assigment[j-1];
			  k2 = assigment[i-1];
			  S1.clear();
			  S2.clear();
			  for(int ii=0; ii<routes[k1].size(); ++ii)
				  cout<<routes[k1][ii]<<" ";
			  cout<<endl;
			  for(int ii=0; ii<routes[k2].size(); ++ii)
				  cout<<routes[k2][ii]<<" ";
			  cout<<endl;
			  for(int ii=ord[j-1]; ii<ord[l-1]; ++ii)
				  S1.push_back(routes[k1][ii]);
			  for(int ii=ord[m-1]; ii<ord[i-1]; ++ii)
				  S2.push_back(routes[k2][ii]);

			  /*for(int ii=0;ii<S1.size();ii++)
				  cout<<S1[ii]<<" ";
			  cout<<endl;
			  for(int ii=0;ii<S2.size();ii++)
				  cout<<S2[ii]<<" ";
			  cout<<endl;*/
			  addFeasCut(data, routingMDL, S1, S2, i, j, l, m, k1, k2);
			  return true;
			}
		}
	}

	return false;
}

int addFeasCut(InputData& data, MDL_Route& mdl, vector<int>& S1, vector<int>& S2, int i, int j, int l, int m, int k1, int k2)
{
	GRBLinExpr expr = 0;
	int size;

    for(int ii=0; ii<=S1.size()-1; ii++)
	{
      for(int jj=0; jj<=S1.size()-1; jj++)
        if(ii!=jj)
          expr += mdl.x[k1][S1[ii]][S1[jj]];
	  expr += mdl.x[k1][S1[ii]][l];
	}

    for(int ii=0; ii<=S2.size()-1; ii++)
	{
      for(int jj=0; jj<=S2.size()-1; jj++)
		if(ii!=jj)
          expr += mdl.x[k2][S2[ii]][S2[jj]];
	  expr += mdl.x[k2][S2[ii]][i];
	}

	size = (int)S1.size()+(int)S2.size()-1;
	//cout<<Si.size()<<" "<<Sj.size()<<endl;
	//cout<<"[FC]"<<expr<<"<="<<size<<endl;
	mdl.mdl->addConstr(expr<=size);
	mdl.mdl->update();
	fsCuts++;
	return 1;
}


bool findCuts(InputData& data, MDL_Route& routingMDL, vector< vector<int> >& routes, vector<int>& assigment, int ub)
{
	int i,j,ki,kj;
	bool flag, ret;
	int sum1,sum2;
	vector<int> Si;
	vector<int> Sj;

	ret = false;
	for(vector<pair<int, int> >::iterator ii=data.Phi.begin(); ii!=data.Phi.end(); ++ii)
	{
		i = ii->first;
		j = ii->second;
		ki = assigment[i-1];
		kj = assigment[j-1];

		if(ki != kj)
		{
		  Si.clear();
		  flag = false;
		  sum1 = 0;
		  //cout<<"("<<i<<","<<j<<")"<<endl;
		  for (vector<int>::iterator it=routes[ki].begin(); it!=routes[ki].end(); ++it)//&& !flag
		  {
			//cout<<*it<<" ";
			if(*it!=0 && *it!=data.tasks+1 && !flag)
			{
			  sum1+=data.P[*it-1];
			  if(*it==i)
			    Si.push_back(0);
			  else
				Si.push_back(*it);
			}
			if(*it==i)
			 flag = true;
		  }
		  //cout<<endl;

		  Sj.clear();
		  flag = false;
		  sum2 = data.P[j-1];
		  if(sum1+sum2<ub)
		    for (vector<int>::iterator it=routes[kj].begin(); it!=routes[kj].end(); ++it)
		    {
			  //cout<<*it<<" ";
		      if(*it==j)
			    flag = true;
			  if(*it!=0 && *it!=data.tasks+1 && *it!=j && flag)
			  {
		        Sj.push_back(*it);
				sum2+=data.P[*it-1];
			    if(sum1+sum2>ub)
				  flag = false;
			  }
		    }
		  //cout<<endl;
          //cout<<sum1<<" "<<sum2<<" "<<sum1+sum2<<" "<<ub<<endl;
		  if(sum1+sum2>ub)
		  {
			/*for(vector<int>::iterator it=Si.begin(); it!=Si.end(); ++it)
			  cout<<*it<<" ";
			cout<<endl;
			for(vector<int>::iterator it=Sj.begin(); it!=Sj.end(); ++it)
			  cout<<*it<<" ";
			cout<<endl;*/
			addPrecCut(data, routingMDL, Si, Sj, i, j, ki, kj);
			ret = true;
		  }
		}
		//cout<<endl;
	}

	return ret;
}

int addPrecCut(InputData& data, MDL_Route& m, vector<int>& Si, vector<int>& Sj, int i, int j, int ki, int kj)
{
	GRBLinExpr expr = 0;
	int l;

    for(int ii=0; ii<Si.size(); ii++)
	{
      for(int jj=0; jj<=Si.size()-1; jj++)
        if(ii!=jj && Si[jj]!=0)
          expr += m.x[ki][Si[ii]][Si[jj]];
	  expr += m.x[ki][Si[ii]][i];
	}

    for(int ii=0; ii<Sj.size(); ii++)
	{
      for(int jj=0; jj<=Sj.size()-1; jj++)
		if(ii!=jj && Sj[ii]!=data.tasks+1)
          expr += m.x[kj][Sj[ii]][Sj[jj]];
	  expr += m.x[kj][j][Sj[ii]];
	}

	l = (int)Si.size()+(int)Sj.size()-1;
	//cout<<Si.size()<<" "<<Sj.size()<<endl;
	//cout<<"[PC]"<<expr<<"<="<<l<<endl;
	m.mdl->addConstr(expr<=l);
	m.mdl->update();
	pcCuts++;
	return 1;
}

int createRoutingModel(InputData& data, MDL_Route& m)
{
    int N, K, t, d, T, B;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

	ostringstream str;

    /**Variables declarations*/
    for(int k=0; k<=K-1; k++)     //k in Cranes
    {
      m.x[k] = new GRBVar*[N+1];    //x^k_i i in Tasks+0
      for(int i=0; i<=N; i++)     //i in Tasks+0
      {
        m.x[k][i] = m.mdl->addVars(N+2, GRB_BINARY);
        m.mdl->update();
        for(int j=1; j<=T; j++)   //j in Tasks+T
        {
          str<<"x_("<<i<<","<<j<<","<<k+1<<")";
          m.x[k][i][j].set(GRB_StringAttr_VarName, str.str());
          //cout<<str.str()<<endl;
          str.str("");
          str.clear();
          if(i==j)
            m.x[k][i][j].set(GRB_DoubleAttr_UB, 0.0);
        }
      }
      for(int i=0; i<=N; i++)     //i in Tasks+0
        m.x[k][i][0].set(GRB_DoubleAttr_UB, 0.0);
    }

    m.y = new GRBVar*[N];
    for(int i=0; i<=N-1; i++)
    {
      m.y[i] = m.mdl->addVars(K, GRB_BINARY);
      m.mdl->update();
      for(int k=0; k<=K-1; k++)
      {
        str<<"y_("<<i+1<<","<<k+1<<")";
        m.y[i][k].set(GRB_StringAttr_VarName, str.str());
        str.str("");
        str.clear();
      }
    }

    m.eta = m.mdl->addVar(0.0, GRB_INFINITY, 1, GRB_CONTINUOUS, "eta");  //makespan
    m.mdl->update();
    
    /**Constraints declarations*/
    GRBLinExpr expr = 0;

    //out degree 0
    for(int k=0; k<=K-1; k++)  //one ctr. for each crane
    {
      expr = 0;
      for(int j=1; j<=T; j++)  //j in Tasks+T
        expr += m.x[k][0][j];
      m.mdl->addConstr(expr==1);
    }

    //in degree T
    for(int k=0; k<=K-1; k++)  //one ctr. for each crane
    {
      expr = 0;
      for(int j=0; j<=N; j++)  //j in Tasks+0
        expr += m.x[k][j][T];
      m.mdl->addConstr(expr==1);
    }

    //assignment constraints
    for(int i=1; i<=N; i++)       //i in Tasks
      for(int k=0; k<=K-1; k++)
      {
        expr = 0;
        for(int j=1; j<=T; j++)   //j in Tasks+T
          if(i!=j)
            expr += m.x[k][i][j];
        m.mdl->addConstr(expr-m.y[i-1][k]==0);

        expr = 0;
        for(int j=0; j<=N; j++)   //j in Tasks+0
          if(i!=j)
            expr += m.x[k][j][i];
        m.mdl->addConstr(expr-m.y[i-1][k]==0);
      }

    //flow conservation
    for(int i=0; i<=N-1; i++) //i in Tasks
    {
      expr = 0;
      for(int k=0; k<=K-1; k++)
        expr += m.y[i][k];
      m.mdl->addConstr(expr==1);
    }

    //MinMax approach
    for(int k=0; k<=K-1; k++)
    {
      expr = 0;
      for(int j=1; j<=N; j++)  //j in Tasks
        expr += m.x[k][0][j]*(t*MOD(data.L0[k]-data.L[j-1]) + data.P[j-1]);
      
      for(int i=1; i<=N; i++)  //i,j in Tasks
        for(int j=1; j<=N; j++)
          if(i!=j)
            expr += m.x[k][i][j]*(t*MOD(data.L[i-1]-data.L[j-1]) + data.P[j-1]);

      m.mdl->addConstr(expr<=m.eta);
    }

    //SECS for S: |S|=2
    for(int i=1; i<=N; i++)  //i,j in Tasks
      for(int j=i+1; j<=N; j++)
        if(i!=j)
        {
          for(int k=0; k<=K-1; k++)
          {
            expr = m.x[k][i][j] + m.x[k][j][i];
            m.mdl->addConstr(expr<=1);
          }
        }

	//precedence cycle?
    for(int i=0; i<N; i++)
	{
       for(int j=i+1; j<N; j++)
	   {
		   if(data.predec[i].size()>0 && data.predec[j].size()>0 && data.L[i]!=data.L[j])
		   {
			   for(vector<int>::iterator ii=data.predec[i].begin(); ii!=data.predec[i].end(); ++ii)
				   for(vector<int>::iterator jj=data.predec[j].begin(); jj!=data.predec[j].end(); ++jj)
					   for(int k=0; k<K; k++)
					   {
					     expr = m.x[k][*ii][j+1]+m.x[k][*jj][i+1];
						 m.mdl->addConstr(expr<=1);
						 //cout<<expr<<endl;
					   }
		   }
	   }
	}

	/*impose lower and upper bay limits for a crane*/
	int min, max;
	for(int k=1; k<=K; k++)
	{
	  min = (k-1)*(d+1)+1;
	  max = B - (K-k)*(1+d);
	  expr = 0;
	  for(int i=0; i<N; i++)
		  if(data.L[i]<min || data.L[i]>max)
		    expr += m.y[i][k-1];
	  m.mdl->addConstr(expr==0);
	}
	
    m.mdl->update();

    /*for(int k=0; k<=K-1; k++)
      m.xVars.push_back(m.x[k]);
    m.mdl->getEnv().set(GRB_IntParam_LazyConstraints, 1);
    MyCallBack callback = MyCallBack(&data,m.xVars,m.y);
    m.mdl->setCallback(&callback);*/

	//m.mdl->write("formulation.lp");

	return 1;
}

double solveRoutingModel(InputData& data, MDL_Route& m, vector< vector<int> >& routes, vector<int>& assigment)
{
    int N, K, t, d, T, B;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    m.mdl->optimize();
    double ret = 0;
    if(m.mdl->get(GRB_IntAttr_SolCount)>0)
    {
      ret = m.eta.get(GRB_DoubleAttr_X);
      //cout<<"MinMax found:"<<m.eta.get(GRB_DoubleAttr_X)<<endl; 

      for(int k=0; k<=K-1; k++)
        for(int u=0; u<=N; u++)
          solutionT[k][u] = m.mdl->get(GRB_DoubleAttr_X, m.x[k][u], N+2);

	  for(int u=0; u<N; u++)
	   solutionA[u] = m.mdl->get(GRB_DoubleAttr_X, m.y[u], K);

      int u;
      bool flag;
      for(int k=0; k<=K-1; k++)
      {
        //cout<<"Task sequence for crane "<<k<<endl;
        u = 0; flag = false;
        while(!flag)
        {
          for(int j=1; j<=T; j++)
          {
            if(solutionT[k][u][j]>0.9)
            {
              //cout<<"x_("<<u<<","<<j<<","<<k+1<<")="<<solutionT[k][u][j]<<endl;
              u = j; j = T+1;
              if(u==T)
                flag=true;
              else
			  {
                assigment[u-1] = k;
				routes[k].push_back(u);
			  }
            }
          }
        }
      }
    }//if soluton found....
	return ret;
}

int addNoGood(InputData& data, MDL_Route& m, vector< vector<int> >& routes)
{
	GRBLinExpr expr;
	int u=0;
	int l=0;
	vector<int>::iterator aux, back;
	vector<int> S;
	bool flag;

	expr = 0;
	for(int k=0; k<data.cranes; k++)
	{
		u = 0;
		routes[k].push_back(data.tasks+1);
		for (vector<int>::iterator it=routes[k].begin(); it!=routes[k].end(); ++it)
		{
			aux=it;
			back=it;
			++aux;
			flag = false;
			while(aux!=routes[k].end() && *aux!=data.tasks+1 && data.L[*aux-1]==data.L[*it-1])//TODO: check corner situations
			{
				S.push_back(*it);
				++it; ++aux;
				flag = true;
			}
			if(flag)
			{
			   S.push_back(*it);
			   /*cout<<u<<" ";
			   for(int i=0;i<S.size(); i++)
				   cout<<S[i]<<" ";
			   cout<<*aux<<endl;*/
			   for(int i=0;i<S.size(); i++)
			   {
				   expr += m.x[k][u][S[i]];
				   for(int j=0;j<S.size(); j++)
					   if(i!=j)
						   expr += m.x[k][S[i]][S[j]];
				   expr += m.x[k][S[i]][*aux];
			   }
			   l+=(int)S.size()+1;
			   u=*aux;
			   it=aux;//back;
			   S.clear();
			}
			else
			{
			  expr += m.x[k][u][*it];
              u=*it;
			  ++l;
			}
		}
		//expr += m.x[k][u][data.tasks+1];//u->T
		//++l;
	}

	//cout<<"[NG]"<<expr<<"<="<<l-1<<endl;
	m.mdl->addConstr(expr<=l-1);
	m.mdl->update();
	noGood++;
	return 1;
}

int addNoGoodCrane(InputData& data, MDL_Route& m, vector<int>& route, int k)
{
	GRBLinExpr expr;
	int u;
	int l;

	expr = 0;
	l = 1;
	u = 0;
	for (vector<int>::iterator it=route.begin(); it!=route.end(); ++it)
	{
		expr += m.x[k][u][*it];
		u=*it;
		++l;
	}
	expr += m.x[k][u][data.tasks+1];//u->T
	//cout<<expr<<"<="<<l-1<<endl;
	m.mdl->addConstr(expr<=l-1);
	m.mdl->update();
	return 1;
}

int createSchedulligModel(InputData& data, MDL_Schedule& m, vector< vector<int> >& seq, int M)
{
    int N, K, t, d, T, B, aux;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    ostringstream str;
	    
	//scheduling variables
    m.W = m.mdl->addVar(0, M, 1, GRB_CONTINUOUS, "W");  //makespan
    //D = mdl.addVars(N, GRB_INTEGER);//TODO:INTEGER?
    m.D = m.mdl->addVars(NULL, NULL, NULL, NULL, NULL, N);
	m.C = m.mdl->addVars(K, GRB_CONTINUOUS);//TODO:INTEGER?

    m.z = new GRBVar*[N];
    for(int i=1; i<=N; i++)
    {
      m.z[i-1] = m.mdl->addVars(N, GRB_BINARY);
      m.mdl->update();
      for(int j=1; j<=N; j++)
      {
        str<<"z_("<<i<<","<<j<<")";
        m.z[i-1][j-1].set(GRB_StringAttr_VarName, str.str());
        str.str("");
        str.clear();
        if(i==j)
          m.z[i-1][j-1].set(GRB_DoubleAttr_UB,0.0);
      }
    }

    m.mdl->update();
    for(int i=0; i<N; i++)
	{
	  m.D[i].set(GRB_DoubleAttr_Obj, 0);
	  m.D[i].set(GRB_CharAttr_VType, GRB_CONTINUOUS);
	  //C[k].set(GRB_DoubleAttr_LB, 0.0);
	  //C[k].set(GRB_DoubleAttr_UB, M);
      str<<"D_("<<i+1<<")";
      m.D[i].set(GRB_StringAttr_VarName, str.str());
      str.str("");
      str.clear();
	}

	for(int k=0; k<K; k++)
	{
      str<<"C_("<<k+1<<")";
      m.C[k].set(GRB_StringAttr_VarName, str.str());
      str.str("");
      str.clear();
	}

    //Constraints declarations
	GRBLinExpr expr = 0;

    //s.t. nonSim{(i,j) in Psi}: z[i,j] + z[j,i] = 1;
    for(int i=1; i<=N; i++)
      for(int j=i+1; j<=N; j++)
        if(MOD(data.L[i-1]-data.L[j-1])<=data.margin && MOD(data.L[i-1]-data.L[j-1])>0)
        {
          expr = m.z[i-1][j-1]+m.z[j-1][i-1];
          m.mdl->addConstr(expr==1);
        }

    //s.t. precedence relations (i,j) => z_ij=1 z_ji=0
    for(vector< pair<int,int> >::iterator ele = data.Phi.begin(); ele!=data.Phi.end(); ++ele)
    {
      //cout<<ele->first<<"<"<<ele->second<<endl;
      m.z[ele->first-1][ele->second-1].set(GRB_DoubleAttr_LB, 1.0);
      m.z[ele->first-1][ele->second-1].set(GRB_DoubleAttr_UB, 1.0);
      m.z[ele->second-1][ele->first-1].set(GRB_DoubleAttr_LB, 0.0);
      m.z[ele->second-1][ele->first-1].set(GRB_DoubleAttr_UB, 0.0);
    }

    //pass throug _x, get the sequence for each crane and set the
    //z[i,j] bonunds for tasks on the same sequence i.e.
    //z_ij=1, z_ji=0 if 0----i-----j----T
    for(int k=0; k<=K-1; k++)
    {//the sequence is ordered...
      for(int ii=0; ii<seq[k].size(); ii++)
        for(int jj=ii+1; jj<seq[k].size(); jj++)
        {
          //cout<<seq[k][ii]<<" "<<seq[k][jj]<<endl;
          m.z[seq[k][ii]-1][seq[k][jj]-1].set(GRB_DoubleAttr_LB, 1.0);
          m.z[seq[k][ii]-1][seq[k][jj]-1].set(GRB_DoubleAttr_UB, 1.0);
          m.z[seq[k][jj]-1][seq[k][ii]-1].set(GRB_DoubleAttr_LB, 0.0);
          m.z[seq[k][jj]-1][seq[k][ii]-1].set(GRB_DoubleAttr_UB, 0.0);
        }
      //cout<<endl;
    }

//task schedulling...
    //s.t. _cc1{i in Tasks, j in Tasks, k in Cranes: i!=j}: D[i] + t*abs(l[i]-l[j]) + p[j] - D[j] <= M*(1-_x[i,j,k]);
    for(int i=1; i<=N; i++)
      for(int j=1; j<=N; j++)
        if(i!=j)
          for(int k=0; k<=K-1; k++)
          {
			if(solutionT[k][i][j]>0.1)
			{
		      str<<"R1_"<<i<<","<<j;
			  aux = t*MOD(data.L[i-1]-data.L[j-1]) - data.a_[j-1];
              expr = m.D[i-1]+t*MOD(data.L[i-1]-data.L[j-1])+data.P[j-1]-m.D[j-1];
			  m.mdl->addConstr(expr<=M*(1-solutionT[k][i][j]), str.str());
			  str.str("");
              str.clear();
			}
          }

    //s.t. _cc2{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[i] + p[j] - D[j] <= M*(1-z[i,j]);
    //s.t. _cc3{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[j] - p[j] - D[i] <= M*z[i,j];
    for(int i=1; i<=N; i++)
      for(int j=1; j<=N; j++)
      {
        if(i!=j)
        {
          str<<"R2_"<<i<<","<<j;
          expr = m.D[i-1] + data.P[j-1] - m.D[j-1] + M*m.z[i-1][j-1];
          m.mdl->addConstr(expr<=M, str.str());
		  str.str("");
          str.clear();

		  str<<"R3_"<<i<<","<<j;
          expr = m.D[j-1] - data.P[j-1] - m.D[i-1] - (M-data.P[j-1]-data.P[i-1])*m.z[i-1][j-1];//(M-data.P[j-1]-data.a_[i-1]-data.P[i-1])
          m.mdl->addConstr(expr<=0, str.str());
		  str.str("");
          str.clear();
        }
      }

    //s.t. _cc6{j in Tasks, k in Cranes:lT[k]!=0}: D[j] + t*abs(l[j]-lT[k]) - C[k] <= M*(1-_x[j,T,k]);
    for(int j=1; j<=N; j++)  //(l_j-lT_k)==0
      for(int k=0; k<=K-1; k++)
      {
        expr = m.D[j-1] - m.C[k];
		str<<"R6_"<<j<<","<<k;
        m.mdl->addConstr(expr<=M*(1-solutionT[k][j][T]), str.str());
		str.str("");
        str.clear();
      }

	//cout<<"lower bound on starting time\n";
	/*for(int i=0; i<N; i++)
	  cout<<data.a_[i]<<" ";
	cout<<endl;*/
    //s.t. _cc8{j in Tasks, k in Cranes}: r[k] - D[j] + t*abs(l0[k]-l[j]) + p[j] <= M*(1-_x[O,j,k]);
    for(int j=1; j<=N; j++)
      for(int k=0; k<=K-1; k++)
      {
		str<<"R8";
        expr = data.R[k] - m.D[j-1] + t*MOD(data.L0[k]-data.L[j-1]) + data.P[j-1];
		m.mdl->addConstr(expr<=(data.R[k]+t*MOD(data.L0[k]-data.L[j-1])-data.a_[j-1])*(1-solutionT[k][0][j]), str.str());
		str.str("");
        str.clear();
      }

    //s.t. cc9{k in Cranes}: C[k]<=W;
    for(int k=0; k<=K-1; k++)
	{
	  str<<"R9_"<<k;
      m.mdl->addConstr(m.C[k]<=m.W, str.str());
      str.str("");
      str.clear();
	}

    //s.t. cc10{i in Tasks}: D[i]<=U;
    for(int i=1; i<=N; i++)
	{
	  str<<"R10a_"<<i;
      m.mdl->addConstr(m.D[i-1]<=M, str.str());
	  str.str("");
      str.clear();
	  str<<"R10b_"<<i;
	  m.mdl->addConstr(m.D[i-1]>=data.LB[i-1]+data.P[i-1], str.str());
	  str.str("");
      str.clear();
	}

	//minimum temporal distance (Bierwirth, Meisel)
	for(int i=1; i<=N; i++)
	{
	  for(int j=i+1; j<=N; j++)
	  {
		for(int v=1; v<=K; v++)
		  for(int w=1; w<=K; w++)
		  {
			aux = data.delta(i,j,v,w);
		    if(v!=w && aux>0)
			{
			  //cout<<i<<" "<<j<<" "<<v<<" "<<w<<"="<<aux<<endl;
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += solutionA[i-1][v-1] + solutionA[j-1][w-1];
			  expr += m.z[i-1][j-1];
			  expr = (M+aux)*expr;
			  expr += m.D[i-1] + aux + data.P[j-1] - m.D[j-1];
			  str<<"RT_1_"<<i<<","<<j<<","<<v<<","<<w;
			  m.mdl->addConstr(expr<=3*(M+aux), str.str());
         	  str.str("");
              str.clear();
			  //
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += solutionA[i-1][v-1] + solutionA[j-1][w-1];
			  expr += m.z[j-1][i-1];
			  expr = (M+aux)*expr;
			  expr += m.D[j-1] + aux + data.P[i-1] - m.D[i-1];
			  str<<"RT_2_"<<i<<","<<j<<","<<v<<","<<w;
			  m.mdl->addConstr(expr<=3*(M+aux), str.str());
         	  str.str("");
              str.clear();
			  //--------------------------
			  expr = 0;
			  //for(int u=0; u<=N; u++)
			  //{
			  //  if(u!=i)
			  //    expr += x[v-1][u][i];
			  //  if(u!=j)
			  //    expr += x[w-1][u][j];
			  //}
			  expr += solutionA[i-1][v-1] + solutionA[j-1][w-1];
			  expr += -m.z[i-1][j-1]-m.z[j-1][i-1];
			  //str<<"RT_3_"<<i<<","<<j<<","<<v<<","<<w;
			  m.mdl->addConstr(expr<=1, str.str());
         	  str.str("");
              str.clear();
			}
		  }
	  }
	}

	//valid inequality (51)
	for(int k=0; k<K; k++)
	{
	  expr = data.R[k];
	  for(int i=1; i<=N; i++)
	  {
		if(solutionT[k][0][i]>0.1)
		{
		  expr += (data.LB[i-1])*solutionT[k][0][i];//+ data.LB[i-1]-data.P[i-1]
		  //cout<<"x("<<k<<","<<0<<","<<i<<") "<<t*(MOD(data.L0[k]-data.L[i-1]))<<" "<<data.LB[i-1]<<endl;
		}

		for(int j=1; j<=N; j++)
		  if(i!=j)
			if(solutionT[k][i][j]>0.1)
			{
		      expr += (data.P[i-1] + t*MOD(data.L[i-1]-data.L[j-1]))*solutionT[k][i][j];
			  //cout<<"x("<<k<<","<<i<<","<<j<<") "<<data.P[i-1]<<" "<<t*MOD(data.L[i-1]-data.L[j-1])<<endl;
			}

		if(solutionT[k][i][T]>0.1)
		{
		  expr += data.P[i-1]*solutionT[k][i][T];
		  //cout<<"x("<<k<<","<<i<<","<<T<<") "<<data.P[i-1]<<endl;
		}
	  }
	  m.mdl->addConstr(m.C[k]>=expr);
	}

    m.mdl->update();
    //m.mdl->write("formulation.lp");

	return 1;
}


bool solveSchedullingModel(InputData& data, MDL_Schedule& m, int& sol)
{
    int N, K, t, d, T, B;
    N = data.tasks;
    K = data.cranes;
    t = data.time;
    d = data.margin;
    T = N+1;
	B = data.bays;

    m.mdl->optimize();
    if(m.mdl->get(GRB_IntAttr_SolCount)>0)
    { 
      //vector<int> assigment(N);
	  //vector<int> finish;
      double makespan;

	  finish.clear();

      int u;
      bool flag;
      for(int k=0; k<=K-1; k++)
      {
        //cout<<"Task sequence for crane "<<k<<endl;
        u = 0; flag = false;
        while(!flag)
        {
          for(int j=1; j<=T; j++)
          {
            if(solutionT[k][u][j]>0.9)
            {
              //cout<<"x_("<<u<<","<<j<<","<<k+1<<")="<<solutionT[k][u][j]<<endl;
              u = j; j = T+1;
              if(u==T)
                flag=true;
              else
                assigment[u-1] = k;
            }
          }
        }
      }

      solutionC[0] = (int)(m.W.get(GRB_DoubleAttr_X)+0.5);
      //cout<<"Makespan="<<solutionC[0]<<endl;
      makespan = solutionC[0];

      solutionC = m.mdl->get(GRB_DoubleAttr_X, m.C, K);
      //for(int k=1; k<=K; k++)
      //  cout<<"C_"<<k<<"="<<solutionC[k-1]<<endl;

      solutionD = m.mdl->get(GRB_DoubleAttr_X, m.D, N);
      for(int i=1; i<=N; i++)
      {
        //cout<<"D_"<<i<<"="<<solutionZ[0][i-1]<<endl;
        finish.push_back((int)(solutionD[i-1]+0.5));
      }

      for(int i=0; i<=N-1; i++)
      {
        solutionZ[i] = m.mdl->get(GRB_DoubleAttr_X, m.z[i], N);
        //for(int j=0; j<=N-1; j++)
        //  if(solutionZ[i][j]>0)
        //    cout<<"z_("<<i+1<<","<<j+1<<")="<<solutionZ[i][j]<<endl;
      }

	  /*if(draw==1)
        drawScheduleSVG(data, finish, assigment, (int)(makespan));*/
	  sol = (int)makespan;
	  return true;
    }//if soluton found....

	return false;
}

int drawScheduleSVG(InputData& data, vector<int>& D, vector<int>& assigment, int makeSpan, const char* inst)
{
  int yMax;
  string colors[10]={"#dcdcdc", "#a52a2a", "#006400", "#ffa500", "#ff0000",
                     "#ffff00", "#ff4500", "#008000", "#ffebeb", "#808080" };

  string name(inst);
  name = name.substr(0, name.rfind("."));
  ostringstream str;
  str<<name<<"b_"<<++COUNT<<".svg";
  //cout<<inst<<endl;
  //cout<<str.str()<<endl;
  ofstream svgFile(str.str());

  yMax = data.bays*55;
  svgFile<<"<?xml version=\"1.0\" standalone=\"yes\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">\n";
  svgFile<<"<svg xmlns=\"http://www.w3.org/2000/svg\">\n";
  //draws the time axis
  svgFile<<"<line x1=\"0\"  y1=\""<<yMax<<"\" x2=\""<<10*makeSpan+55<<"\" y2=\""<<yMax<<"\" style=\"stroke:#000000; stroke-width: 6px;\"/>\n";

  //draws a vertical line for each task completion time
  for(int i=0; i<=data.tasks-1; i++)
  {
   svgFile<<"<line x1=\""<<10*D[i]<<"\" y1=\""<<yMax<<"\" x2=\""<<10*D[i]<<"\"   y2=\"0\" style=\"stroke:#000000; stroke-width: 2px; stroke-dasharray: 10 5;\"/>\n";
   svgFile<<"<text fill=\"BLACK\" font-size=\"30\" x=\""<<10*D[i]<<"\" y=\""<<yMax+20<<"\">"<<D[i]<<"</text>\n";
  }

  //draws a rectangle for each task and the task number inside it
  int start;
  for(int i=0; i<=data.tasks-1; i++)
  {
    start = D[i]-data.P[i];    
    svgFile<<"<rect rx=\"13\" x=\""<<10*start<<"\" y=\""<<(data.L[i]-1)*55<<"\" height=\"50\" width=\""<<10*data.P[i]<<"\" style=\"fill:"<<colors[assigment[i]]<<"\"/>\n";
    svgFile<<"<text fill=\"black\" font-size=\"30\" x=\""<<10*data.P[i]/2 + 10*start<<"\" y=\""<<(data.L[i]-1)*55+35<<"\">"<<i+1<<"</text>\n";
  }
  svgFile<<"</svg>\n";
  return 0;
}
