#include "io.h"

int MOD_(int a) { return a>0 ? a : -a; }

InputData::InputData(string filePath)
{
  ifstream dataFile(filePath.c_str());
  string line;
  vector<int> values;
  int aux;

  if(!dataFile)
    throw std::invalid_argument("Data file could not be opened!\n");

  safeGetline(dataFile, line); //get the first line with n,b,|Psi|,|Phi|,q,t,s
  split(line, ',', values);
  if(values.size() != 7)
    throw std::invalid_argument("1:Invalid format of data file...\n");

  tasks = values[0];
  bays = values[1];
  int numPrecs = values[2];
  int numNonsim = values[3];
  cranes = values[4];
  time = values[5];
  margin = values[6];
  cout<<endl;

  values.clear();
  safeGetline(dataFile, line); //get the processing times
  split(line, ',', values);
  if(values.size() != tasks)
    throw std::invalid_argument("2:Invalid format of data file...\n");
  for(int i=0; i<tasks; i++)
  {
    P.push_back(values[i]);
	LB.push_back(0);
  }

  values.clear();
  safeGetline(dataFile, line); //get the bay locations
  split(line, ',', values);
  if(values.size() != tasks)
    throw std::invalid_argument("3:Invalid format of data file...\n");
  for(int i=0; i<tasks; i++)
    L.push_back(values[i]);

  values.clear();
  safeGetline(dataFile, line);  //location - deck or hold
  split(line, ',', values);
  if(values.size() != tasks)
    throw std::invalid_argument("3.1:Invalid format of data file...\n");
  for(int i=0; i<tasks; i++)
    H.push_back(values[i]);

  values.clear();
  safeGetline(dataFile, line);  //operation - loading or unloading
  split(line, ',', values);
  if(values.size() != tasks)
    throw std::invalid_argument("3.2:Invalid format of data file...\n");
  for(int i=0; i<tasks; i++)
    O.push_back(values[i]);

  values.clear();
  safeGetline(dataFile, line); //get the ready times
  split(line, ',', values);
  if(values.size() != cranes)
    throw std::invalid_argument("4:Invalid format of data file...\n");
  for(int i=0; i<cranes; i++)
    R.push_back(values[i]);

  values.clear();
  safeGetline(dataFile, line); //get the initial location
  split(line, ',', values);
  if(values.size() != cranes)
    throw std::invalid_argument("5:Invalid format of data file...\n");
  for(int i=0; i<cranes; i++)
    L0.push_back(values[i]);

  predec = new vector<int>[tasks];
  values.clear();
  safeGetline(dataFile, line); //marker "Phi" on sixth line
  if(line.compare("Phi:") !=0 )
    throw std::invalid_argument("6:Invalid format of data file...\n");
  for(int i=0; i<numPrecs;i++)
  {
    safeGetline(dataFile, line); //read one pair
    split(line, ',', values);
    Phi.push_back(make_pair(values[0], values[1]));
    predec[values[0]-1].push_back(values[1]);
    values.clear();
  }

  values.clear();
  safeGetline(dataFile, line); //marker "Psi" on sixth line
  if(line.compare("Psi:") != 0)
    throw std::invalid_argument("7:Invalid format of data file...\n");
  for(int i=0; i<numNonsim;i++)
  {
    safeGetline(dataFile, line); //read one pair
    split(line, ',', values);
    Psi.push_back(make_pair(values[0], values[1]));
    values.clear();
  }

  //build the Phi set and the predecessors of each task
  //predec = new vector<int>[tasks];
  for(int i=1; i<=tasks; i++)
    for(int j=1; j<=tasks; j++)
      if(i!=j && L[i-1]==L[j-1])  //i and j in the same bay
      {
        if(O[i-1]==2)          //i is unloading
        {
          if(O[j-1]==1)        //j is loading at the same bay!
          {
            predec[j-1].push_back(i);
            Phi.push_back(make_pair(i,j));
          }
          else if(O[j-1]==2)            //j is unloading
          {
            if(H[i-1]==1 && H[j-1]==2)  //i is in deck and j is in hold
            {
              predec[j-1].push_back(i);
              Phi.push_back(make_pair(i,j));
            }
          }
        }
        else if(O[i-1]==1)                        //i is loading
        {
          if(O[j-1]==1 && H[j-1]==1 && H[i-1]==2) //j is loading at deck and i at hold
          {
            predec[j-1].push_back(i);
            Phi.push_back(make_pair(i,j));
          }
        }
      }

  cout<<"predecessors\n";
  for(int i=0; i<=tasks-1; i++)
  {
    cout<<"task "<<i+1<<": ";
    if(predec[i].size()>0)
    {
      for(int j=0; j<=predec[i].size()-1; j++)
	  {
		//Preds.push_back(make_pair(predec[i][j],i+1));
        cout<<predec[i][j]<<" ";
	  }
    }
    cout<<endl;
  }

  for(int i=0; i<tasks; i++)
  {	
	aux = 99999;
	for(int k=0; k<cranes; k++)
	  if(R[k]+time*MOD_(L0[k]-L[i])<aux)
	    aux = R[k]+time*MOD_(L0[k]-L[i]);
   for(int j=0; j<predec[i].size(); j++)
     aux += P[predec[i][j]-1];
	a_.push_back(aux);
  }
}

/*test*/
/*int InputData::alfa(int task)
{
  int r = 0;
  for(int j=0; j<predec[task].size(); j++)
  {
	if(LB[predec[task][j]-1]!=-1)
	  r+=LB[predec[task][j]-1];
	else
      r += alfa(predec[task][j]-1);
  }
  //r += P[task];
  LB[task] = r;
  return r;
}*/

void InputData::alfa(int task)
{
  int r = 0;
  for(int j=0; j<predec[task].size(); j++)
	  r += P[predec[task][j]-1];
  
  int aux = 999999;//MOD_(L[task]-L0[0]);
  for(int k=1; k<=cranes; k++)
  {
    if(L[task]>=(k-1)*(margin+1)+1 && L[task]<=bays-(cranes-k)*(1+margin))
      if(aux>MOD_(L[task]-L0[k-1]))
        aux = MOD_(L[task]-L0[k-1]);
  }
  //r += P[task];
  LB[task] = r+aux;
}

/*int InputData::lbStart(int task)
{
  int ret = 0;
  int max = 0;

  for(int j=0; j<predec[task].size(); j++)
  {
    if(a[predec[task][j]-1]==-1)
	  a[predec[task][j]-1] = lbStart(predec[task][j]);
	if(max < a[predec[task][j]-1]+P[predec[task][j]-1])
	  max = a[predec[task][j]-1]+P[predec[task][j]-1];
  }

  ret = a_[task] <= max ? a_[task] : max;
}*/

int InputData::delta(int i, int j, int v, int w)
{
  if(v<w && L[i-1]>L[j-1]-(margin+1)*(w-v))
    return time*(L[i-1]-L[j-1]+(margin+1)*(w-v));
  else if(v>w && L[i-1]<L[j-1]+(margin+1)*(v-w))
	     return time*(L[j-1]-L[i-1]+(margin+1)*(v-w));
  return 0;
}

InputData::~InputData()
{}

//split string by a given delimiter
void InputData::split(const string &s, char delim, vector<int> &elems)
{
    stringstream ss(s);
    string item;
    
    while (getline(ss, item, delim))
      elems.push_back(atoi(item.c_str()));
}

//read a line from file correctly whatever the ending convention "\r", "\n" and "\r\n"
istream& InputData::safeGetline(istream& is, string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    istream::sentry se(is, true);
    streambuf* sb = is.rdbuf();

    for(;;)
    {
        int c = sb->sbumpc();
        switch (c)
        {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

ostream& operator<<(ostream& outstr, const InputData& id)
{
  cout<<"number of tasks:"<<id.tasks<<endl;
  cout<<"number of bays:"<<id.bays<<endl;
  cout<<"number of cranes:"<<id.cranes<<endl;
  cout<<"travel time:"<<id.time<<endl;
  cout<<"safety margin:"<<id.margin<<endl;

  for(int i=1;i<=id.tasks;i++)
    cout<<i<<"\t";
  cout<<endl;
  for(int i=0; i<id.P.size(); i++)
    cout<<id.P[i]<<"\t";
  cout<<endl;
  for(int i=0; i<id.L.size(); i++)
    cout<<id.L[i]<<"\t";
  cout<<endl;
  for(int i=0; i<id.H.size(); i++)
    if(id.H[i]==1)
     cout<<"d\t";
    else if(id.H[i]==2)
     cout<<"h\t";
  cout<<endl;
  for(int i=0; i<id.O.size(); i++)
    if(id.O[i]==1)
     cout<<"l\t";
    else if(id.O[i]==2)
     cout<<"u\t";
  cout<<endl<<endl;

  for(int i=1;i<=id.cranes;i++)
    cout<<i<<"\t";
  cout<<endl;
  for(int i=0; i<id.R.size(); i++)
    cout<<id.R[i]<<"\t";
  cout<<endl;
  for(int i=0; i<id.L0.size(); i++)
    cout<<id.L0[i]<<"\t";
  cout<<endl;

  for(int i=0; i<id.Phi.size(); i++)
    cout<<"("<<id.Phi[i].first<<","<<id.Phi[i].second<<") ";
  cout<<endl;
  for(int i=0; i<id.Psi.size(); i++)
    cout<<"("<<id.Psi[i].first<<","<<id.Psi[i].second<<") ";
  cout<<endl;
  return outstr;
}
