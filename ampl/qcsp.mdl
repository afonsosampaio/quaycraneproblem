param N integer; #tasks
param K integer; #cranes
param B integer; #bays
param t integer; #travel time
param d integer; #safety margin

set Tasks ordered := {1..N};
set Cranes := {1..K};
set Arcs := {i in Tasks, j in Tasks: i!=j};

param p{Tasks};   #processing times
param r{Cranes};  #ready times of cranes
param l{Tasks};   #bay location of tasks
param l0{Cranes}; #initial position of cranes
param lT{Cranes}; #final position of cranes
param ttask{Tasks}; #type of task
param toper{Tasks}; #type of operation
param colors{Cranes};  #to draw the svg schedule...

param O := 0;
param T := N+1;

set _Phi := {(i,j) in Arcs: l[i]==l[j]};
set Phi := {(i,j) in _Phi: (toper[i]==2 and toper[j]==1) or (toper[i]==2 and toper[j]==2 and ttask[i]==1 and ttask[j]==2) or (toper[i]==1 and toper[j]==1 and ttask[i]==2 and ttask[j]==1)};
set Psi := {(i,j) in Arcs: abs(l[i]-l[j])<=d and i<j};

set ArcsT := {i in Tasks union {0}, j in {T}};
set ArcsO := {j in {O}, i in Tasks union {T}};
set Arcs_ := Arcs union ArcsO union ArcsT;

#binary vars
var x{(i,j) in Arcs_,k in Cranes} binary;
var y{i in Tasks, k in Cranes} binary;
var z{(i,j) in Arcs} binary;

#continuous vars
var W >=0;         #makespan
var D{Tasks} >=0;  #completion of task
var C{Cranes} >=0; #completion of crane
var e >=0;

param U := sum{i in Tasks} p[i] + (2*l0[1]-2)*t + (2*(B-l0[1]+1)-2)*t;
param M := 3*U;#999;

#ILP - the routing of the cranes
minimize route: e;# + sum{i in Tasks, j in Tasks: i!=j} z[i,j];
s.t. d0{k in Cranes}: sum {j in Tasks union {T}} x[O,j,k] = 1;
s.t. dT{k in Cranes}: sum {j in Tasks union {O}} x[j,T,k] = 1;
s.t. asg1{i in Tasks, k in Cranes}: sum{j in Tasks union {T}: i!=j} x[i,j,k] = y[i,k];
s.t. asg2{i in Tasks, k in Cranes}: sum{j in Tasks union {O}: i!=j} x[j,i,k] = y[i,k];
s.t. flowAsg{i in Tasks}: sum{k in Cranes} y[i,k] = 1;
s.t. nonCross{i in Tasks, j in Tasks, k in Cranes: i!=j and l[i]<l[j]}:
      sum{v in Cranes, u in Tasks union {O}: v<=k and u!=j} x[u,j,v] +
      sum{v in Cranes, u in Tasks union {O}: v>=k and v<=K and u!=i} x[u,i,v] <= 1+z[i,j]+z[j,i];
s.t. nonSim{(i,j) in Psi}: z[i,j] + z[j,i] = 1;
s.t. preced1{(i,j) in Phi}: z[i,j] = 1;
s.t. preced2{(i,j) in Phi}: z[j,i] = 0;

#minmax constraints
s.t. minmax{k in Cranes}: sum{j in Tasks} x[O,j,k]*(t*abs(l0[k]-l[j])+p[j]) +
                    sum{i in Tasks, j in Tasks:i!=j} x[i,j,k]*(t*abs(l[i]-l[j])+p[j]) +
                    sum{i in Tasks:lT[k]!=0} x[i,T,k]*(t*abs(l[i]-lT[k])) <= e;

#LP - the schedullign of tasks
minimize makespan: W;# + sum{k in Cranes} C[k];
s.t. cc1{i in Tasks, j in Tasks, k in Cranes: i!=j}: D[i] + t*abs(l[i]-l[j]) + p[j] - D[j] <= M*(1-x[i,j,k]);

s.t. cc2{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[i] + p[j] - D[j] <= M*(1-z[i,j]);
s.t. cc3{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[j] - p[j] - D[i] <= M*z[i,j];

s.t. cc4{i in Tasks, j in Tasks: i!=j and l[i]==l[j]}: D[j] - p[j] - D[i] -
       sum{k in Cranes, u in Tasks:l[u]!=l[i] and u!=j} t*x[u,j,k] -
       sum{k in Cranes:l0[k]!=l[i]} t*x[O,j,k] <= M*z[i,j];

s.t. cc5{i in Tasks, j in Tasks: i!=j and l[i]==l[j]}: D[i] + p[j] - D[j] +
       sum{k in Cranes, u in Tasks:l[u]!=l[i] and u!=j} t*x[u,j,k] +
       sum{k in Cranes:l0[k]!=l[i]} t*x[O,j,k] <= M*(1-z[i,j]);

s.t. cc6{j in Tasks, k in Cranes:lT[k]!=0}: D[j] + t*abs(l[j]-lT[k]) - C[k] <= M*(1-x[j,T,k]);
s.t. cc7{j in Tasks, k in Cranes:lT[k]==0}: D[j] - C[k] <= M*(1-x[j,T,k]);
s.t. cc8{j in Tasks, k in Cranes}: r[k] - D[j] + t*abs(l0[k]-l[j]) + p[j] <= M*(1-x[O,j,k]);
s.t. cc9{k in Cranes}: C[k]<=W;
s.t. cc10{i in Tasks}: D[i]<=U;

s.t. cc11: W<=104;

#the schedulling problem with the x variables fixed (param _x)
param _x{(i,j) in Arcs_,k in Cranes};
#param _z{(i,j) in Arcs};

s.t. _cc1{i in Tasks, j in Tasks, k in Cranes: i!=j}: D[i] + t*abs(l[i]-l[j]) + p[j] - D[j] <= M*(1-_x[i,j,k]);

s.t. _cc2{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[i] + p[j] - D[j] <= M*(1-z[i,j]);
s.t. _cc3{i in Tasks, j in Tasks: i!=j and l[i]!=l[j]}: D[j] - p[j] - D[i] <= M*z[i,j];

s.t. _cc4{i in Tasks, j in Tasks: i!=j and l[i]==l[j]}: D[j] - p[j] - D[i] -
       sum{k in Cranes, u in Tasks:l[u]!=l[i] and u!=j} t*_x[u,j,k] -
       sum{k in Cranes, u in {O}:l0[k]!=l[i]} t*_x[O,j,k] <= M*z[i,j];

s.t. _cc5{i in Tasks, j in Tasks: i!=j and l[i]==l[j]}: D[i] + p[j] - D[j] +
       sum{k in Cranes, u in Tasks:l[u]!=l[i] and u!=j} t*_x[u,j,k] +
       sum{k in Cranes, u in {O}:l0[k]!=l[i]} t*_x[O,j,k] <= M*(1-z[i,j]);

s.t. _cc6{j in Tasks, k in Cranes:lT[k]!=0}: D[j] + t*abs(l[j]-lT[k]) - C[k] <= M*(1-_x[j,T,k]);
s.t. _cc7{j in Tasks, k in Cranes:lT[k]==0}: D[j] - C[k] <= M*(1-_x[j,T,k]);
s.t. _cc8{j in Tasks, k in Cranes}: r[k] - D[j] + t*abs(l0[k]-l[j]) + p[j] <= M*(1-_x[O,j,k]);

s.t. _nonCross{i in Tasks, j in Tasks, k in Cranes: i!=j and l[i]<l[j]}:
      sum{v in Cranes, u in Tasks union {O}: v<=k and u!=j} _x[u,j,v] +
      sum{v in Cranes, u in Tasks union {O}: v>=k and v<=K and u!=i} _x[u,i,v] <= 1+z[i,j]+z[j,i];

#=============================
param Delta{Tasks,Tasks,Cranes,Cranes};
s.t. temp1{i in Tasks, j in Tasks, v in Cranes, w in Cranes: i<j and Delta[i,j,v,w]>0}: D[i]+Delta[i,j,v,w]+p[j]-D[j] <= M*(3-z[i,j]-sum{u in Tasks union {O}:u!=i}x[u,i,v]-sum{u in Tasks union {O}:u!=j}x[u,j,w]);

s.t. temp2{i in Tasks, j in Tasks, v in Cranes, w in Cranes: i<j and Delta[i,j,v,w]>0}: D[j]+Delta[i,j,v,w]+p[i]-D[i] <= M*(3-z[j,i]-sum{u in Tasks union {O}:u!=i}x[u,i,v]-sum{u in Tasks union {O}:u!=j}x[u,j,w]);

s.t. temp3{i in Tasks, j in Tasks, v in Cranes, w in Cranes: i<j and Delta[i,j,v,w]>0}: sum{u in Tasks union {O}:u!=i}x[u,i,v]+sum{u in Tasks union {O}:u!=j}x[u,j,w] <= 1+z[i,j]+z[j,i];
#=============================


#generate all subtours (only for testing...)
set S ordered := 1 .. (2**N - 1);
set Cutset {s in S} := {i in Tasks: (s div 2**(ord(i)-1)) mod 2 = 1};
s.t. subtour{s in S, k in Cranes: card(Cutset[s]) >= 2 and card(Cutset[s]) <= N/2}:
  sum {(i,j) in Arcs: (i in Cutset[s]) and (j in Cutset[s])} x[i,j,k] <= card(Cutset[s])-1;

