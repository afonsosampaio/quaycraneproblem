import sys
import re

# Run with 'parser.py [file] [num]'
# where [file] is a txt file containing the instances in the format
# {li,                //The location of task i
#  Type of task,      //Deck=1 or Hold=2
#  Pi,                //The time required to perform task i
#  Type of operation, //loading=1 or discharging=2 
#  {0,0,0,0,0},       //Do not consider 
#  0,                 //Do not consider
# }
# (as in the original instance file by Young-Man Park)
# and [num] is the initial file number...

def main(argv):
  rfile = open(argv[0],'r')
  rawData = rfile.read()
  rlist = re.split(r'\n{2,}', rawData)
  fileCount = int(argv[1])
  for elem in rlist:
    match = re.findall(r'{(\d+),(\d+),(\d+),(\d+),(\S+)},', elem)
    tmpfile = open('k'+str(fileCount)+'.qcs', 'w')
    line1 = ''
    line2 = ''
    line3 = ''
    line4 = ''
    for m in match:
      line1 = line1 + m[2] + ','
      line2 = line2 + m[0] + ','
      line3 = line3 + m[1] + ','
      line4 = line4 + m[3] + ','
    tmpfile.write(line1+'\n'+line2+'\n'+line3+'\n'+line4+'\n')
    tmpfile.close()
    fileCount = fileCount + 1
  exit(0)

if __name__ == "__main__":
  main(sys.argv[1:])

