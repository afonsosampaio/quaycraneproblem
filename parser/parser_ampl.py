import sys
import re

# Run with 'parser.py [file] [num]'
# where [file] is a txt file containing the instances in the format
# {li,                //The location of task i
#  Type of task,      //Deck=1 or Hold=2
#  Pi,                //The time required to perform task i
#  Type of operation, //loading=1 or discharging=2 
#  {0,0,0,0,0},       //Do not consider 
#  0,                 //Do not consider
# }
# (as in the original instance file by Young-Man Park)
# and [num] is the initial file number for output names (k13,k14,...)

def main(argv):
  rfile = open(argv[0],'r')
  rawData = rfile.read()
  rlist = re.split(r'\n{2,}', rawData)
  fileCount = int(argv[1]);
  for elem in rlist:
    match = re.findall(r'{(\d+),(\d+),(\d+),(\d+),(\S+)},', elem)
    tmpfile = open('k'+str(fileCount)+'.dat', 'w')
    task = 0
    for m in match:
      task = task + 1
      #ampl
      tmpfile.write(str(task)+' '+m[2]+' '+m[0]+' '+m[1]+' '+m[3]+'\n')
    tmpfile.close()
    fileCount = fileCount + 1
  exit(0)

if __name__ == "__main__":
  main(sys.argv[1:])

